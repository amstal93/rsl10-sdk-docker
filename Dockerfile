# rsl10-sdk-docker project (https://gitlab.com/arturmadrzak/rsl10-sdk-docker)
# Copyright (c) 2020 Artur Mądrzak <artur@madrzak.eu>

FROM ubuntu:bionic

MAINTAINER "Artur Mądrzak <artur@madrzak.eu>"

ARG version

ENV RSLSDK /opt/RSL10_SDK

RUN test -n "${version:?Provide existing pack version in --build-arg}"

RUN apt-get update -qq && \
    apt-get install -y -qq software-properties-common && \
    add-apt-repository ppa:team-gcc-arm-embedded/ppa && \
    apt-get update -qq && \
    apt-get install -y -qq build-essential \
                           curl \
                           gcc-arm-embedded \
                           unzip

COPY sdk/ONSemiconductor.RSL10."${version}".pack /tmp/sdk.pack
RUN unzip -qq /tmp/sdk.pack -d "${RSLSDK}" && \
    rm -rf /tmp/sdk.pack
